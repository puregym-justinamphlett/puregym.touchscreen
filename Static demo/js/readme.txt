animsition = transitions when entering/exiting a page
iscroll = imitate mobile-style touch powered vertical scrolling with page bounce
jquery.backgroundposition = the constantly moving background layers
micromodal = pop up modals
vegas = index page automated fullscreen slider