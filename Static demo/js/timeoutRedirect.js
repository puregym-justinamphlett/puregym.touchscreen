
    (function() {

        var timeoutSeconds = 120;
        var interactionEvents = ['touchstart', 'touchend', 'touchmove', 'mousedown', 'mouseup', 'mousemove', 'keydown', 'keyup'];
        var splashPaths = ['index.html', ''];
        var timeout = null;

        function resetTimeout() {
            clearTimeout(timeout);

            if(isOnSplashLocation() === true) {
                return;
            }

            timeout = setTimeout(redirectToSplashLocation, convertToMilliseconds(timeoutSeconds));
        }

        function isOnSplashLocation() {
            var isOnSplashLocation = false;

            splashPaths.forEach(function(splashPath) {
                if (window.location.pathname.toLowerCase() === '/' + splashPath) {
                    isOnSplashLocation = true;
                }
            });

            return isOnSplashLocation;
        }

        function redirectToSplashLocation() {
            window.location.href = splashPaths[0];
        }

        function convertToMilliseconds(seconds) {
            return seconds * 1000;
        }

        function init() {
            interactionEvents.forEach(function(eventName) {
                window.addEventListener(eventName, resetTimeout);
            })
            resetTimeout();
        }

        init();

    })();

